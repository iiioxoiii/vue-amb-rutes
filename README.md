# experiment-v1

## Project setup
```
L'exercici consisteix en una instal·lació de vue amb el mòdul route i axios.
 
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
