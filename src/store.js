
// Magatzem de dades
// El id no es fa servir. Les dades usades son el nom de la ciutat
// i el slug (o troç de la url que fa servir el modul route)

export default {
        
	capitals: [
	
        {"id": 1, "nom": "Barcelona", "slug":"barcelona"},
        {"id": 2, "nom": "Berlin", "slug":"berlin"},
        {"id": 3, "nom": "Johannesburg", "slug":"johannesburg"},
        {"id": 4, "nom": "London", "slug":"london"},
        {"id": 5, "nom": "Bruges", "slug":"bruges"},
        {"id": 6, "nom": "Seattle", "slug":"seattle"},
        {"id": 7, "nom": "Jerusalem", "slug":"jerusalem"},
        {"id": 8, "nom": "Tokyo", "slug":"tokyo"},
        {"id": 9, "nom": "Havana", "slug":"havana"},
        {"id": 10, "nom": "Sydney", "slug":"sydney"},
        {"id": 11, "nom": "Antananarivo", "slug":"antananarivo"},
        {"id": 12, "nom": "Bangkok", "slug":"bangkok"},
        {"id": 13, "nom": "Belgrade", "slug":"belgrade"},
        {"id": 14, "nom": "Mexico City", "slug":"mexico-city"},
        {"id": 15, "nom": "Madrid", "slug":"madrid"},
        {"id": 16, "nom": "Paris", "slug":"paris"},
        {"id": 17, "nom": "Melbourne", "slug":"melbourne"},
        {"id": 18, "nom": "Moscow", "slug":"moscow"},
        {"id": 19, "nom": "Pekin", "slug":"pekin"},
        {"id": 20, "nom": "Marrakech", "slug":"marrakech"},
        {"id": 21, "nom": "Hiroshima", "slug":"hiroshima"},
        {"id": 22, "nom": "Argel", "slug":"argel"},
        {"id": 23, "nom": "Viena", "slug":"viena"},
        {"id": 24, "nom": "Dhaka", "slug":"dhaka"},
        {"id": 25, "nom": "Managua", "slug":"managua"},
        {"id": 26, "nom": "Rome", "slug":"rome"},
        {"id": 27, "nom": "Freetown", "slug":"freetown"},
        {"id": 28, "nom": "Hanoi", "slug":"hanoi"},
        {"id": 29, "nom": "Kingston", "slug":"kingston"},
        {"id": 30, "nom": "Pyongyang", "slug":"pyongyang"}]
}